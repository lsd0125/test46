<?php
require __DIR__. '/__connect_db.php';


$sid = isset($_GET['sid']) ? intval($_GET['sid']) : 0;

if($sid > 0) {
    $sql = "DELETE FROM `address_book` WHERE `sid` = $sid";

    $mysqli->query($sql);

    $_SESSION['flashMsg'] = array(
        'type' => 'danger',
        'msg' => '項目刪除完成: '. $sid,
    );

}
header('Location: show_data.php');