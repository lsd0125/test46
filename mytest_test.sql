-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- 主機: 127.0.0.1
-- 產生時間： 2016-08-17 03:29:41
-- 伺服器版本: 10.1.13-MariaDB
-- PHP 版本： 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `mytest`
--
CREATE DATABASE IF NOT EXISTS `mytest` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mytest`;

-- --------------------------------------------------------

--
-- 資料表結構 `address_book`
--

CREATE TABLE `address_book` (
  `sid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `address_book`
--

INSERT INTO `address_book` (`sid`, `name`, `email`, `phone`, `birthday`, `address`) VALUES
(1, '史大巴4-22--', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(2, '史大巴4-22--', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(5, '史大巴4-22--', 'star13@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(6, '史大巴4-22--', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(7, '史大巴4-22--', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(9, '史大巴10', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(10, '史大巴11', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(11, '史大巴12', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(12, '史大巴13----', 'star13@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(13, '史大巴14', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(14, '史大巴15', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(15, '史大巴16', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(16, '史大巴16', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(17, '史大巴21', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(18, '史大巴22', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(19, '史大巴23', 'star13@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(20, '史大巴24', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(21, '史大巴25', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(22, '史大巴26', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(23, '史大巴30', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(24, '史大巴31', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(25, '史大巴32', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(26, '史大巴33', 'star13@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(27, '史大巴27', 'star@gmail.com', '0918777888', '2016-08-11', '台北市中山區'),
(29, '史大巴36', 'star@gmail.com', '0918111222', '2016-08-11', '台北市中山區'),
(33, '林阿德', 'uy0327@yahoo.com.tw', '39458349', '1989-01-01', '新北市'),
(34, '大大', 'lskdf@skldfj.com', '28973429', '0000-00-00', '<script>alert(''爛芭樂'')</script>');

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `address_book`
--
ALTER TABLE `address_book`
  ADD PRIMARY KEY (`sid`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `address_book`
--
ALTER TABLE `address_book`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
