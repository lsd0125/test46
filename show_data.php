<?php
require __DIR__. '/__connect_db.php';
$per_page = 6;

$page_num = isset($_GET['page_num']) ? intval($_GET['page_num']) : 1;


$t_result = $mysqli->query("SELECT 1 FROM `address_book`");
$num_rows = $t_result->num_rows;
$total_pages = ceil($num_rows/$per_page);


$sql = sprintf("SELECT * FROM `address_book` ORDER BY sid DESC LIMIT %s, %s", ($page_num-1)*$per_page, $per_page);
$result = $mysqli->query($sql);


// $row = $result->fetch_assoc();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">
    <style>
        .glyphicon-remove-sign {
            color: red;
            font-size: 20px;
        }
        .glyphicon-pencil {
            color: blue;
            font-size: 20px;
        }
    </style>
</head>
<body>

<div class="container">



    <div style="height:50px">
        <?php if(isset($_SESSION['flashMsg'])):?>
        <div class="alert alert-<?= $_SESSION['flashMsg']['type'] ?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?= $_SESSION['flashMsg']['msg'] ?>
        </div>
        <?php
        unset($_SESSION['flashMsg']);

        endif; ?>
    </div>

    <div class="col-lg-6" style="margin-bottom: 20px">
        <a class="btn-primary btn" href="insert_data.php">新增</a>
    </div>

    <div class="col-lg-12">
        <table class="table table-striped table-bordered">
            <tr>
                <td>sid</td>
                <td>name</td>
                <td>email</td>
                <td>phone</td>
                <td>birthday</td>
                <td>address</td>
                <td>edit</td>
                <td>delete</td>
            </tr>
            <?php while( $row = $result->fetch_assoc() ): ?>
            <tr>
                <td><?= $row['sid'] ?></td>
                <td><?= $row['name'] ?></td>
                <td><?= $row['email'] ?></td>
                <td><?= $row['phone'] ?></td>
                <td><?= $row['birthday'] ?></td>
                <td><?= htmlentities($row['address']) ?></td>
                <td><a href="edit_data.php?sid=<?= $row['sid'] ?>">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a></td>

                <?php /*
                <td><a href="delete_data.php?sid=<?= $row['sid'] ?>">
                    */ ?>
                <td><a href="javascript:delete_it(<?= $row['sid'] ?>)">
                        <span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
                    </a></td>
            </tr>
            <?php endwhile; ?>
        </table>
    </div>

    <div class="col-lg-12">
    <nav aria-label="">
        <ul class="pagination">
            <?php for($i=1; $i<=$total_pages; $i++): ?>
            <li class="<?= $page_num==$i ? 'active' : '' ?>">
                <a href="?page_num=<?= $i ?>"><?= $i ?></a>
            </li>
            <?php endfor; ?>
        </ul>
    </nav>
    </div>
<?php
/*
    for($i=1; $i<=$total_pages; $i++):
        if($page_num==$i) {
            printf('%s&nbsp;&nbsp;', $i);
        } else {
            printf('<a href="?page_num=%s">%s</a>&nbsp;&nbsp;', $i, $i);
        }
    endfor;
*/
?>
<!--
    <div>
        <?php
        if($page_num==1){
            echo '&lt;&lt;&nbsp;&nbsp;';
        } else {
            echo '<a href="?page_num=1">&lt;&lt;</a>&nbsp;&nbsp;';
        }

        if($page_num==1){
            echo '&lt;&nbsp;&nbsp;';
        } else {
            echo '<a href="?page_num='. ($page_num-1). '">&lt;</a>&nbsp;&nbsp;';
        }

        echo "$page_num &nbsp;";

        if($page_num==$total_pages){
            echo '&gt;&nbsp;&nbsp;';
        } else {
            echo '<a href="?page_num='. ($page_num+1). '">&gt;</a>&nbsp;&nbsp;';
        }

        if($page_num==$total_pages){
            echo '&gt;&gt;&nbsp;&nbsp;';
        } else {
            echo '<a href="?page_num='. $total_pages. '">&gt;&gt;</a>&nbsp;&nbsp;';
        }
        ?>
    </div>
    -->
</div>
<script src="js/jquery-3.1.0.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>
<script>
    function delete_it(sid) {
        if(confirm("確定要刪除編號為 " + sid + " 的資料嗎?")){
            location.href = "delete_data.php?sid=" + sid;
        }
    }


</script>
</body>
</html>