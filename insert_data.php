<?php

require __DIR__. '/__connect_db.php';

if(isset($_POST['name'])) {
/*
 * // 第一種用法
    $sql = sprintf("INSERT INTO `address_book`(
`sid`, `name`, `email`, `phone`, `birthday`, `address`
) VALUES (NULL, '%s', '%s', '%s', '%s', '%s')",
        $mysqli->escape_string( $_POST['name'] ),
        $mysqli->escape_string( $_POST['email'] ),
        $mysqli->escape_string( $_POST['phone'] ),
        $mysqli->escape_string( $_POST['birthday'] ),
        $mysqli->escape_string( $_POST['address'] )
        );

    //echo $sql;

    $mysqli->query($sql);
*/

    $statement = $mysqli->prepare("INSERT INTO `address_book`(
`sid`, `name`, `email`, `phone`, `birthday`, `address`
) VALUES (NULL, ?, ?, ?, ?, ?)");

    $statement->bind_param("sssss",
        $_POST['name'],
        $_POST['email'],
        $_POST['phone'],
        $_POST['birthday'],
        $_POST['address']
        );
    $statement->execute();
    $statement->close();



    header('Location: show_data.php');

    exit;





}


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">
</head>
<body>

<div class="container">
    <div class="col-lg-6">
    <div class="panel panel-default" style="margin-top: 50px">
        <div class="panel-heading">
            <h3 class="panel-title">新增資料</h3>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" name="name" placeholder="name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">email</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="email" name="email" placeholder="email">
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone" class="col-sm-2 control-label">phone</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="phone">
                    </div>
                </div>
                <div class="form-group">
                    <label for="birthday" class="col-sm-2 control-label">birthday</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="birthday" name="birthday" placeholder="birthday">
                    </div>
                </div>
                <!--
                <div class="form-group">
                    <label for="myid" class="col-sm-2 control-label">birtdsdfsdfhday</label>
                    <div class="col-sm-10">
                        <input type="checkbox" class="form-control" id="myid">
                    </div>
                </div>
                -->
                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">address</label>
                    <div class="col-sm-10">
                        <textarea  class="form-control" name="address" id="address" cols="30" rows="10"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">新增</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
</div>
<script src="js/jquery-3.1.0.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>
</body>
</html>