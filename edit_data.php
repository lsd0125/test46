<?php

require __DIR__. '/__connect_db.php';


$sid = isset($_GET['sid']) ? intval($_GET['sid']) : 0;

if(isset($_POST['sid']) and isset($_POST['name'])) {

    $statement = $mysqli->prepare("UPDATE `address_book` 
        SET `name`=?,`email`=?,`phone`=?,`birthday`=?,`address`=? 
        WHERE `sid`=?");

    $statement->bind_param("sssssi",
        $_POST['name'],
        $_POST['email'],
        $_POST['phone'],
        $_POST['birthday'],
        $_POST['address'],
        $_POST['sid']
        );
    $statement->execute();
    $statement->close();

    $_SESSION['flashMsg'] = array(
        'type' => 'info',
        'msg' => '項目修改完成: '. $sid,
    );

    header('Location: '. $_SESSION['referer']);
    exit;
} else {
    $_SESSION['referer'] = $_SERVER['HTTP_REFERER'];
}



$sql = "SELECT * FROM `address_book` WHERE `sid`=$sid";
$result = $mysqli->query($sql);
$row = $result->fetch_assoc();


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css">
</head>
<body>

<div class="container">
    <div class="col-lg-6">
    <div class="panel panel-default" style="margin-top: 50px">
        <div class="panel-heading">
            <h3 class="panel-title">編輯資料 <?= $_SERVER['HTTP_REFERER'] ?></h3>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post">
                <input type="hidden" name="sid" value="<?=$row['sid']?>">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" name="name" value="<?=$row['name']?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">email</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="email" name="email" value="<?=$row['email']?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone" class="col-sm-2 control-label">phone</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="phone" name="phone" value="<?=$row['phone']?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="birthday" class="col-sm-2 control-label">birthday</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="birthday" name="birthday" value="<?=$row['birthday']?>">
                    </div>
                </div>
                <!--
                <div class="form-group">
                    <label for="myid" class="col-sm-2 control-label">birtdsdfsdfhday</label>
                    <div class="col-sm-10">
                        <input type="checkbox" class="form-control" id="myid">
                    </div>
                </div>
                -->
                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">address</label>
                    <div class="col-sm-10">
                        <textarea  class="form-control" name="address" id="address" cols="30" rows="10"><?=$row['address']?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">編輯</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
</div>
<script src="js/jquery-3.1.0.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>
</body>
</html>